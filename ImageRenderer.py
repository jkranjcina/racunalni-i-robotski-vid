# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 09:58:46 2022

@author: vub
"""

import matplotlib.pylab as plt
import cv2
import numpy as np


image = cv2.imread('road.jpg')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

print(image.shape)
height = image.shape[0]
width = image.shape[1]

region_of_interest_vertices = [
    (0, height),
    (width/2, height/2),
    (width, height)
]

def region_of_interest(img, vertices):
    mask = np.zeros_like(img)
    channel_count = img.shape[2]
    match_mask_color = (225,) * channel_count
    cv2.fillPoly(mask, vertices, match_mask_color)
    masked_image = cv2.bitwise_and(img, maks)
    return maked_image

cropepd_iamge = region_of_interest(image,np.array([region_of_interest_vertices )]))
plt.imshow(image)
plt.show()